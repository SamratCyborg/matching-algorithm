import {MongoDB} from "../util/mongodb";
import {Connection, Model, Schema} from "mongoose";
import {JobSchema} from "../schemas/job.schema";
import * as mongoose from "mongoose";
import {SchemaDefinitions} from "../schemas";
import {Page} from "../models/request";
import {McgSchemaDefinitions} from "../mcg/schemas";

export class Repository {
    private entityName: string;
    private context: Model<any>;
    private schemas: any;

    public constructor(_entityName: string) {
        this.entityName = _entityName;
        this.schemas = {...SchemaDefinitions, ...McgSchemaDefinitions }
    }

    private async setContext() {
        if (this.context) return;
        try {
            this.context = await mongoose.model(this.entityName, this.schemas[this.entityName]);
        } catch (e) {
            console.log(e);
        }
    }

    public async find(filter: any, page?: Page) {
        await this.setContext();
        return this.context.find(filter).skip(page ? page.PageSize * page.PageNumber : 0).limit(page ? page.PageNumber : 10);
    }

    public async findOne(filter: any, page?: Page) {
        await this.setContext();
        return this.context.findOne(filter);
    }

    public async aggregate(stages: any[]) {
        await this.setContext();
        return this.context.aggregate(stages);
    }
}
