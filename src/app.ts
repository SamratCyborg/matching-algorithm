import express from "express";
import compression from "compression";  // compresses requests
import bodyParser from "body-parser";
import mongoose from "mongoose";
var cors = require("cors");
import bluebird from "bluebird";
import {MONGODB_URI, SESSION_SECRET} from "./util/secrets";
import {getMatchedCandidates} from "./controllers/candidate-matching.controller";
import {testController} from "./controllers/home.controller";
import {getProjectList} from "./controllers/project.controller";
import {getMatchedJob} from "./controllers/job-matching.controller";
import {getJobList} from "./mcg/controllers/job-list.controller";
import {getOrganizationList} from "./mcg/controllers/organization-list.controller";


// Create Express server
const app = express();


// Connect to MongoDB
const mongoUrl = MONGODB_URI;
mongoose.Promise = bluebird;

mongoose.connect(mongoUrl, {useNewUrlParser: true, useCreateIndex: true}).then(
    () => { /** ready to use. The `mongoose.connect()` promise resolves to undefined. */
    },
).catch(err => {
    console.log("MongoDB connection error. Please make sure MongoDB is running. " + err);
    // process.exit();
});

// Express configuration
app.set("port", process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.get("/", testController);
app.post("/match-candidate", getMatchedCandidates);
app.post("/project-list", getProjectList);
app.post("/match-job", getMatchedJob)
app.post("/job-list", getJobList)
app.post("/organization-list", getOrganizationList)


export default app;
