import {Request, Response} from "express";
import {Repository} from "../repository/repository";
import {Job, ProfiledJob} from "../models/i-job";
import {Experience} from "../models/i-profile";
import {getMatchingFilter} from "../services/filtering.service";
import {getMatchResult} from "../services/matching.service";
import {addPoints, adjustAllCandidate, getMatchPoints} from "../services/scoring.service";
import {extractNecessaryData, removeNullPoints, removeThirdPoint} from "../services/shaking.service";
import {getPercentile} from "../services/ranking.service";
import {Page} from "../models/request";
import {JobSpecificCandidateFilter} from "../models/job-specific-candidate-filter";

export async function getMatchedCandidates(req: Request, res: Response) {
    const jobFilter = {_id: req.body.JobId};
    const jobRepo = new Repository("Job");
    const page: Page = req.body.page || {};
    const candidateFilter: JobSpecificCandidateFilter = req.body.Filter || {};
    const skip = (--page.PageNumber * page.PageSize) || 0;
    const limit = page.PageSize || 10;
    const jobs = await jobRepo.find(jobFilter);
    if (!jobs || !jobs.length) {
        res.send({
            Success: false,
            Message: "No job found with the provided _id"
        })
        return;
    }
    const job: ProfiledJob = jobs[0].toJSON();

    // @ts-ignore
    const experience: Experience = job.Experience;
    experience.ExperienceOnFoW1.SpecialityParents = experience.ExperienceOnFoW1.Specialities.map(item => item.Activity.Parent as string);
    if (experience.ExperienceOnFoW2 && experience.ExperienceOnFoW2.FieldOfWork) {
        experience.ExperienceOnFoW2.SpecialityParents = experience.ExperienceOnFoW2.Specialities.map(item => item.Activity.Parent as string);
    }
    let matchingAggregation: any[] = [];
    const matchingFilter = getMatchingFilter(experience, candidateFilter, job.Address);
    console.log(matchingFilter);
    matchingAggregation.push(matchingFilter);


    matchingAggregation.push(extractNecessaryData(experience));

    matchingAggregation.push({
        $project: {
            _id: 1,
            FoW1: {
                FunctionIds: "$Info.FieldOfWork1.Experience.FunctionIds",
                SpecialityIds: "$Info.FieldOfWork1.Experience.SpecialityIds"
            },
            FoW2: {
                FunctionIds: "$Info.FieldOfWork2.Experience.FunctionIds",
                SpecialityIds: "$Info.FieldOfWork2.Experience.SpecialityIds"
            },
            Info: 1
        }
    });

    matchingAggregation.push({
        $project: {
            "Info.FieldOfWork1.Experience": 0,
            "Info.FieldOfWork2.Experience": 0,
        }
    })
    matchingAggregation.push({
        $addFields: getMatchResult("", experience)
    });

    // adjust 4th speciality

    matchingAggregation.push({
        $addFields: {
            FoW1: {
                SpecialityMatchResult: {
                    $map: {
                        input: "$FoW1.SpecialityMatchResult",
                        as: "item",
                        in: {
                            $cond: {
                                if: {
                                    $gte: ["$$item", 3]
                                },
                                then: 2,
                                else: "$$item"
                            }
                        }
                    }
                }
            },
            FoW2: {
                SpecialityMatchResult: {
                    $map: {
                        input: "$FoW2.SpecialityMatchResult",
                        as: "item",
                        in: {
                            $cond: {
                                if: {
                                    $gte: ["$$item", 3]
                                },
                                then: 2,
                                else: "$$item"
                            }
                        }
                    }
                }
            }
        }
    })

    matchingAggregation.push({
        $addFields: getMatchPoints("", experience)
    });
      matchingAggregation.push(adjustAllCandidate(experience));

     matchingAggregation.push({
         $project: removeNullPoints(experience)
     });


     matchingAggregation = [...matchingAggregation, ...removeThirdPoint(experience),...addPoints(experience),  ...getPercentile(experience) ];

     matchingAggregation.push({
         $match: {
             OverallMatch: {$ne: null}
         }
     });
     matchingAggregation.push({
         $sort: {OverallMatch: -1}
     });
     matchingAggregation.push({
         $project: {
             OverallMatch: 1,
             _id: 1,
             Info: 1,
             FoW1: {
                 SpecialityMatchResult: 1,
                 FunctionMatchResult: 1,
                 MatchPercentage: "$FoW1Match"
             },
             FoW2: {
                 SpecialityMatchResult: 1,
                 FunctionMatchResult: 1,
                 MatchPercentage: "$FoW2Match"
             }
         }
     });
    matchingAggregation.push({
        $facet: {
            TotalCount: [{
                "$count": "Count"
            }],
            Result: [
                {
                    $skip: skip
                },
                {
                    $limit: limit
                }
            ]
        }
    })
    matchingAggregation.push({
        $unwind: "$TotalCount"
    });
    matchingAggregation.push({
        $project: {
            TotalCount: "$TotalCount.Count",
            Result: 1
        }
    })
    const candidateRepo = new Repository("Candidate");
    try {
        const matchResult = await candidateRepo.aggregate(matchingAggregation);
        if (!matchResult.length) {
            res.send({
                Success: true,
                Message: "No match found for this job"
            })
        }

        res.send(matchResult[0]);
    } catch (e) {
        res.send({
            Success: false,
            Message: "Internal Server Error",
            Error: e.toString()
        })
    }

};
