// @ts-ignore
import {Request, Response} from "@types/express";
import {ProjectFilter} from "../models/project-filter";
import {Repository} from "../repository/repository";
import {Page} from "../models/request";
import {JobProjectProjection} from "../models/projections";

export async function getProjectList(req: Request, res: Response) {
    const projectFilter: ProjectFilter = req.body.Filter;
    const projectRepo = new Repository("JobProject");
    const page: Page = req.body.page || {};
    const skip = (page.PageNumber * page.PageSize) || 0;
    const limit = page.PageSize || 10;
    const jobCustomerAndProjectFilter = {} as any;
    jobCustomerAndProjectFilter.$expr = {$gt: [{$size: "$ApplicationIds"}, 0]}
    jobCustomerAndProjectFilter.IsMarkedToDelete = false;
    if (projectFilter.Job) {
        if (projectFilter.Job.ItemId) {
            jobCustomerAndProjectFilter["JobPartial._id"] = projectFilter.Job.ItemId;
        } else if (projectFilter.Job.SearchKey) {
            jobCustomerAndProjectFilter.$or = jobCustomerAndProjectFilter.$or || [];
            jobCustomerAndProjectFilter.$or.push({"JobPartial.Title": new RegExp(projectFilter.Job.SearchKey, "gi")});
            jobCustomerAndProjectFilter.$or.push({"JobPartial.ReferenceNumber": projectFilter.Job.SearchKey});
        }
    }

    if (projectFilter.IsActive) {
        jobCustomerAndProjectFilter["JobPartial.Status"] = "Active";
    }

    if (projectFilter.Customer) {
        if (projectFilter.Customer.ItemId) {
            jobCustomerAndProjectFilter["JobPartial.EmployerPartial.ItemId"] = projectFilter.Customer.ItemId;
        } else if (projectFilter.Customer.SearchKey) {
            jobCustomerAndProjectFilter["JobPartial.EmployerPartial.Name"] = new RegExp(projectFilter.Customer.SearchKey, "gi")
        }
    }
    if (projectFilter.JobOwner) {
        jobCustomerAndProjectFilter["JobPartial.Consultant"] = projectFilter.JobOwner;
    }

    if (projectFilter.CustomerOwner) {
        jobCustomerAndProjectFilter["JobPartial.EmployerPartial.Consultants"] = projectFilter.CustomerOwner;
    }


    if (projectFilter.ProjectStatus) {
        jobCustomerAndProjectFilter["ProjectStatus"] = projectFilter.ProjectStatus;
    }

    if (projectFilter.DateFrom) {
        jobCustomerAndProjectFilter["CreateDate"] = {$gte: projectFilter.DateFrom};
    }
    if (projectFilter.DateTo) {
        jobCustomerAndProjectFilter["CreateDate"] = {$lte: projectFilter.DateTo};
    }

    if (projectFilter.IsRecommended) {
        jobCustomerAndProjectFilter["JobPartial.Consultant"] = "current_person_id";
    }
    const aggregationStages: any[] = [];

    const preFilterAggregation = {
        $match: jobCustomerAndProjectFilter
    }
    aggregationStages.push(preFilterAggregation);


    const jobApplicationFilter: any = {
        $expr: {$eq: ["$$jobId", "$JobId"]},
        IsMarkedToDelete: false
    };
    if (projectFilter.IsRecommended) {
        jobApplicationFilter.Source = "Recommendation";
        jobApplicationFilter.CreatedBy = {$ne: "current_user_id"};
    }
    if (projectFilter.Candidate) {
        if (projectFilter.Candidate.ItemId) {
            jobApplicationFilter["CandidatePartial._id"] = projectFilter.Candidate.ItemId;
        } else if (projectFilter.Candidate.SearchKey) {
            jobApplicationFilter["CandidatePartial.Name"] = new RegExp(projectFilter.Candidate.SearchKey, "gi");
        }
    }
    if (projectFilter.CandidateOwner) {
        console.log(projectFilter.CandidateOwner)
        jobApplicationFilter["CandidatePartial.Consultants._id"] = projectFilter.CandidateOwner
    }

    if (projectFilter.ApplicationStatus) {
        jobApplicationFilter["Status"] = projectFilter.ApplicationStatus;
    }

    if (projectFilter.ApplicationIds && projectFilter.ApplicationIds.length) {
        jobApplicationFilter["_id"]  = {$in: projectFilter.ApplicationIds};
    }

    const applicationLookupAggregation = {
        $lookup: {
            from: "JobApplications",
            let: {jobId: "$JobPartial._id"},
            pipeline: [
                {
                    $match: jobApplicationFilter
                }
            ],
            as: "JobApplications"
        }
    };
    aggregationStages.push(applicationLookupAggregation);
    const removeEmptyApplications = {
        $match: {
            $expr : {$gt: [{$size: "$JobApplications"}, 0]}
        }
    }
    aggregationStages.push(removeEmptyApplications);

    const paginatedResultAggregation: any[] = [
        {
            $skip: skip
        },
        {
            $limit: limit
        }
    ];

    const paginatorAggregation = {
        $facet: {
            TotalCount: [{
                "$count": "Count"
            }],
            Result: paginatedResultAggregation,
            TotalAssignedCount: getTotalAssignedCountAggregation(),
            TotalCandidateCount: getTotalAssignedCandidatesCountAggregation()
        }
    }
    aggregationStages.push(paginatorAggregation);
    aggregationStages.push({
        $unwind: "$TotalCount"
    })
    aggregationStages.push({
        $project: {
            TotalCount: "$TotalCount.Count",
            Result: 1,
            TotalAssignedCount: "$TotalAssignedCount.TotalAssigned",
            TotalCandidateCount: "$TotalCandidateCount.TotalCandidates"
        }
    })
    aggregationStages.push({
        $unwind: "$TotalAssignedCount"
    })
    aggregationStages.push({
        $unwind: "$TotalCandidateCount"
    })

    const populateJobOwnerAggregation: any = {
        $lookup: {
            from: "Persons",
            localField: "JobPartial.Consultant",
            foreignField: "_id",
            as: "JobPartial.Owner"
        }
    };
    paginatedResultAggregation.push(populateJobOwnerAggregation);
    paginatedResultAggregation.push({
        $unwind: {
            path: "$JobPartial.Owner",
            preserveNullAndEmptyArrays: true
        }
    })
    const populateCustomerOwnerAggregation: any = {
        $lookup: {
            from: "Persons",
            localField: "JobPartial.EmployerPartial.Consultants",
            foreignField: "_id",
            as: "JobPartial.EmployerPartial.Owners"
        }
    }
    paginatedResultAggregation.push(populateCustomerOwnerAggregation);
    /*const populateCandidateOwnerAggregation: any = {
        $lookup: {
            from: "Persons",
            localField: "JobApplications.CandidatePartial.Consultants",
            foreignField: "_id",
            as: "JobApplications.Owners"
        }
    };
    aggregationStages.push(populateCandidateOwnerAggregation);*/
    paginatedResultAggregation.push({
        $project: JobProjectProjection
    })

    console.log(aggregationStages);
    const result = await projectRepo.aggregate(aggregationStages);
    res.send({
        Success: true,
        Data: result[0]
    })

}

function getTotalAssignedCountAggregation(): any[] {
    const stages: any[] = [];
    const totalApplicationsPerProject = {
        $project: {
            TotalApplications: {$size: "$ApplicationIds"}
        }
    };
    const totalApplications = {
        $group: {
            _id: null as any,
            TotalAssigned: {$sum: "$TotalApplications"}
        }
    }

    stages.push(totalApplicationsPerProject);
    stages.push(totalApplications);
    stages.push({
        $unwind: "$TotalAssigned"
    });
    stages.push({
        $project: {
            TotalAssigned: 1,
            _id: 0
        }
    })
    return stages;

}

function getTotalAssignedCandidatesCountAggregation(): any[] {
    const stages: any[] = [{
        "$unwind": "$JobApplications"
    }];
    const candidateSetAggregation = {
        $group: {
            _id: null as any,
            candidateIds: {
                $addToSet: "$JobApplications.CandidatePartial._id"
            }
        }
    }
    const totalCandidatesPerProject = {
        $project: {
            TotalCandidates: {$size: "$candidateIds"}
        }
    };
    stages.push(candidateSetAggregation);
    stages.push(totalCandidatesPerProject);
    stages.push({
        $unwind: "$TotalCandidates"
    });
    stages.push({
        $project: {
            TotalCandidates: 1,
            _id: 0
        }
    })


    return stages;
}
