import {Request, Response} from "express";
import {Repository} from "../repository/repository";
import {Job} from "../models/i-job";
import {Page} from "../models/request";
import {ApplicationStatusEnum} from "../constants/application-statuses";
import {MatchedJobProjection} from "../models/matched-job-projection";

export async function getMatchedJob(req: Request, res: Response) {
    const jobRepo = new Repository("Job");
    const page: Page = req.body.page || {};
    const skip = (page.PageNumber * page.PageSize) || 0;
    const limit = page.PageSize || 10;
    let aggregationStages: any[] = [];
    const paginatedResult = [
        {
            $skip: skip
        },
        {
            $limit: limit
        },
        ...getActivityPointIndicatorData()
    ];
    const paginatorAggregation = {
        $facet: {
            TotalCount: [{
                "$count": "Count"
            }],
            Result: paginatedResult,
        }
    }
    aggregationStages = [...aggregationStages, paginatorAggregation];
    aggregationStages.push({
        $unwind: "$TotalCount"
    });
    aggregationStages.push({
        $project: {
            TotalCount: "$TotalCount.Count",
            Result: 1
        }
    })
    const jobs: Job[] = await jobRepo.aggregate(aggregationStages)
    res.send(jobs[0]);
};


function getActivityPointIndicatorData() {
    const stages = [];
    const addProject = {
        $lookup: {
            from: "JobProjects",
            localField: "_id",
            foreignField: "JobPartial._id",
            as: "Project"
        }
    }
    stages.push(addProject);
    stages.push({
        $unwind: {
            path: "$Project",
            preserveNullAndEmptyArrays: true
        }
    });

    const lastActionByOwnerFinder = {
        $lookup: {
            from: "ProjectActions",
            let: {"ownerId": "$Consultant", "projectId": "$Project._id"},
            pipeline: [
                {
                    $sort: {CreateDate: -1}
                },
                {
                    $match: {
                        $expr: {
                            $and: [{$eq: ["$$ownerId", "$PerformedBy._id"]}, {$eq: ["$$projectId", "$ProjectId"]}]
                        }
                    }
                },
                {
                    $limit: 1
                }
            ],
            as: "LastActionByOwner"
        }
    }
    stages.push(lastActionByOwnerFinder);
    stages.push({
        $unwind: {
            path: "$LastActionByOwner",
            preserveNullAndEmptyArrays: true
        }
    });

    const lastAssignmentFinder = {
        $lookup: {
            from: "JobApplications",
            let: {"jobId": "$_id"},
            pipeline: [
                {
                    $match: {
                        $expr: {$eq: ["$$jobId", "$JobId"]}
                    }
                },
                {
                    $sort: {CreateDate: -1}
                },
                {
                    $limit: 1
                }
            ],
            as: "LastAssignment"
        }
    }

    stages.push(lastAssignmentFinder);
    stages.push({
        $unwind: {
            path: "$LastAssignment",
            preserveNullAndEmptyArrays: true
        }
    });
    const targetApplicationStatuses = [ApplicationStatusEnum.SHORT_LISTED, ApplicationStatusEnum.SENT, ApplicationStatusEnum.QUICK_APP]
    const applicationsInShortlistSubmittedOrQuickAppStatusFinder = {
        $lookup: {
            from: "JobApplications",
            let: {"jobId": "$_id"},
            pipeline: [
                {
                    $match: {
                        $expr: {$eq: ["$$jobId", "$JobId"]},
                        Status: {$in: targetApplicationStatuses}
                    }
                },
                {
                    $sort: {CreateDate: -1}
                }
            ],
            as: "ApplicationsInActiveStatus"
        }
    }
    stages.push(applicationsInShortlistSubmittedOrQuickAppStatusFinder);
    stages.push({
        $lookup: {
            from: "Persons",
            localField: "Consultant",
            foreignField: "_id",
            as: "Consultant"
        }
    });
    stages.push({
        $unwind: "$Consultant"
    })
    stages.push({
        $project: MatchedJobProjection
    })
    return stages;
}
