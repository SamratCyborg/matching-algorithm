import {Experience, ExperienceOnFoW} from "../models/i-profile";
import {JobSpecificCandidateFilter} from "../models/job-specific-candidate-filter";
import {Job} from "../models/i-job";
import {subtractMinutes} from "../util/date-operator";
import {Address} from "../models/i-candidate";

function getMatchingFilterForFoW(matchWith: number, experienceOnFoW: ExperienceOnFoW) {
    const specialitiesAndParents = [
        ...experienceOnFoW.SpecialityIds,
        ...experienceOnFoW.Specialities.map(item => item.Activity.Parent)
    ];
    const filterForFoW = {} as any;
    // @ts-ignore
    filterForFoW["Experience.ExperienceOnFoW" + matchWith + ".FieldOfWork.ItemId"] = experienceOnFoW.FieldOfWork.ItemId;
    // @ts-ignore
    filterForFoW["Experience.ExperienceOnFoW" + matchWith + ".FunctionIds"] = {
        $in: experienceOnFoW.FunctionIds
    };
    // @ts-ignore
    filterForFoW["Experience.ExperienceOnFoW" + matchWith + ".SpecialityIds"] = {
        $in: specialitiesAndParents
    };
    if (experienceOnFoW.RequiredActivities && experienceOnFoW.RequiredActivities.length) {
        const requiredFunctionIds = experienceOnFoW.RequiredActivities.filter(item => {
            return experienceOnFoW.FunctionIds.indexOf(item) >= 0;
        });
        const requiredSkillsWithParents: string[][] = experienceOnFoW.RequiredActivities.filter(item => {
            return experienceOnFoW.SpecialityIds.indexOf(item) >= 0;
        }).map(skillId => {
            const skill = experienceOnFoW.Specialities.find(value => {
                return value.Activity.ItemId === skillId;
            });
            return [skillId, skill.Activity.Parent as string];
        });
        const functionMatchingOperation = {} as any;
        functionMatchingOperation['Experience.ExperienceOnFoW' + matchWith + '.ActivityIds'] = {
            $all: requiredFunctionIds
        };
        const skillMatchingOperations = [] as any[];
        requiredSkillsWithParents.forEach(item => {
            const skillMatchingOperation = {} as any;
            skillMatchingOperation['Experience.ExperienceOnFoW' + matchWith + '.ActivityIds'] = {
                $in: item
            };
            skillMatchingOperations.push(skillMatchingOperation);
        })
        // @ts-ignore
        filterForFoW['$and'] = [functionMatchingOperation, ...skillMatchingOperations];
    }
    return filterForFoW;
}

function prepareCandidateFilter(candidateFilter: JobSpecificCandidateFilter): any {
    const filter = {} as any;
    if (candidateFilter.Age && candidateFilter.Age.length) {
        const lowerAgeLimitInMinutes = (candidateFilter.Age[0] || 0) * 12 * 30 * 24 * 60;
        const upperAgeLimitInMinutes = (candidateFilter.Age[1] || 200) * 12 * 30 * 24 * 60;
        const date = new Date();
        const minDOB = subtractMinutes(date, upperAgeLimitInMinutes);
        const maxDOB = subtractMinutes(date, lowerAgeLimitInMinutes);
        filter["DateOfBirth"] = {minDOB, $lte: maxDOB};;
    }
    if (candidateFilter.Budget) {
        filter["Budget"] = {$in: candidateFilter.Budget};
    }
    if (candidateFilter.CandidateStatus) {
        filter["Status"] = candidateFilter.CandidateStatus;
    }
    if (candidateFilter.Country) {
        filter["Address.Country"] = candidateFilter.Country;
    }
    if (candidateFilter.JobType) {
        filter["PreferredJobType"] = candidateFilter.JobType;
    }

    if (candidateFilter.PostCodeFrom || candidateFilter.PostCodeTo) {
        const postCodeRange = [+candidateFilter.PostCodeFrom || 0, +candidateFilter.PostCodeTo || 999999999];
        filter["Address.PostCode"] = {$gte: postCodeRange[0], $lte: postCodeRange[1]};
    }

    if (candidateFilter.PeopleManagement) {
        filter["Leadership"] = {$in: candidateFilter.PeopleManagement};
    }
    if (candidateFilter.Rating) {
        filter["Rating"] = {$in: candidateFilter.Rating};
    }

    if (candidateFilter.CvReg) {
        const lowerTimeLimitInMinutes = (candidateFilter.CvReg[0] || 0) * 12 * 30 * 24 * 60;
        const upperTimeLimitInMinutes = (candidateFilter.CvReg[1] || 200) * 12 * 30 * 24 * 60;
        const date = new Date();
        const minUploadDate = subtractMinutes(date, upperTimeLimitInMinutes);
        const maxUploadDate = subtractMinutes(date, lowerTimeLimitInMinutes);
        filter["Documents.UploadDate"] = {$gte: minUploadDate, $lte: maxUploadDate};
    }
    if (candidateFilter.Language1) {
        filter["Languages"] = {
            $elemMatch: candidateFilter.Language1
        }
    }
    if (candidateFilter.Language2) {
        filter["Languages"] = {
            $elemMatch: candidateFilter.Language2
        }
    }

    return filter;
}

export function getMatchingFilter(experience: Experience, candidateFilter: JobSpecificCandidateFilter, address?: Address) {
    const fows = [experience.ExperienceOnFoW1.FieldOfWork.ItemId];
    if (experience.ExperienceOnFoW2 && experience.ExperienceOnFoW2.FieldOfWork) {
        fows.push(experience.ExperienceOnFoW2.FieldOfWork.ItemId);
    }
    const matchingFilter = {
        "FieldOfWorks.ItemId": {$in: fows},
        $or: [] as any[]
    };
    matchingFilter.$or.push(getMatchingFilterForFoW(1, experience.ExperienceOnFoW1));
    matchingFilter.$or.push(getMatchingFilterForFoW(2, experience.ExperienceOnFoW1));
    if (experience.ExperienceOnFoW2 && experience.ExperienceOnFoW2.FieldOfWork) {
        matchingFilter.$or.push(getMatchingFilterForFoW(2, experience.ExperienceOnFoW2));
        matchingFilter.$or.push(getMatchingFilterForFoW(1, experience.ExperienceOnFoW2));
    }
    const candidateFilterQuery = prepareCandidateFilter(candidateFilter);

    if (candidateFilter && candidateFilter.Distance && address) {
        return {
            $geoNear: {
                near: {
                    type: "Point",
                    coordinates: address.Coordinates,
                },
                distanceField: "calculatedDistance",
                distanceMultiplier: .001,
                maxDistance: candidateFilter.Distance * 1000,
                query: {...matchingFilter, ...candidateFilterQuery}
            }
        }

    } else {
        return {
            $match: {...matchingFilter, ...candidateFilterQuery}
        }
    }
}

