import {Criterion, Experience, PrioritizedActivity} from "../models/i-profile";

interface MatchResultAggregation {
    FoW1: {
        FunctionMatchResult: IndexOfArrayOperation[];
        SpecialityMatchResult: IndexOfArrayOperation[];
        SpecialityParentMatchResult?:  IndexOfArrayOperation[];
    };
    FoW2?: {
        FunctionMatchResult: IndexOfArrayOperation[];
        SpecialityMatchResult: IndexOfArrayOperation[];
        SpecialityParentMatchResult?:  IndexOfArrayOperation[];
    };
}
interface IndexOfArrayOperation {
    $indexOfArray: any[];
}

export function getMatchResult(searchCase: string, experience: Experience): MatchResultAggregation {
    const matchResultAggregation: MatchResultAggregation = {} as any;
    if (experience.ExperienceOnFoW1) {
        matchResultAggregation.FoW1 = {
            FunctionMatchResult:[],
            SpecialityMatchResult: [],
            SpecialityParentMatchResult: []
        } as any;
        experience.ExperienceOnFoW1.Functions.forEach((activity: PrioritizedActivity, index: number) => {
            matchResultAggregation.FoW1.FunctionMatchResult.push({$indexOfArray: ["$FoW1.FunctionIds", experience.ExperienceOnFoW1.FunctionIds[index]]});
        });
        experience.ExperienceOnFoW1.Specialities.forEach((activity: PrioritizedActivity, index: number) => {
            matchResultAggregation.FoW1.SpecialityMatchResult.push({$indexOfArray: ["$FoW1.SpecialityIds", experience.ExperienceOnFoW1.SpecialityIds[index]]});
            matchResultAggregation.FoW1.SpecialityParentMatchResult.push({$indexOfArray: ["$FoW1.SpecialityIds", experience.ExperienceOnFoW1.SpecialityParents[index]]});
        });
    }

    if (experience.ExperienceOnFoW2) {
        matchResultAggregation.FoW2 = {
            FunctionMatchResult:[],
            SpecialityMatchResult: [],
            SpecialityParentMatchResult: []
        } as any;
        experience.ExperienceOnFoW2.Functions.forEach((activity: PrioritizedActivity, index: number) => {
            matchResultAggregation.FoW2.FunctionMatchResult.push({$indexOfArray: ["$FoW2.FunctionIds", experience.ExperienceOnFoW2.FunctionIds[index]]});
        });
        experience.ExperienceOnFoW2.Specialities.forEach((activity: PrioritizedActivity, index: number) => {
            matchResultAggregation.FoW2.SpecialityMatchResult.push({$indexOfArray: ["$FoW2.SpecialityIds", experience.ExperienceOnFoW2.SpecialityIds[index]]});
            matchResultAggregation.FoW2.SpecialityParentMatchResult.push({$indexOfArray: ["$FoW1.SpecialityIds", experience.ExperienceOnFoW2.SpecialityParents[index]]});
        });
    }
    return  matchResultAggregation;

}
