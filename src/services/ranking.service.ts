import {Experience, ExperienceOnFoW} from "../models/i-profile";
const maxScoreMap: number[][] = [
    [80, 141, 141],
    [141, 138, 138],
    [141, 138, 138]
]

export function getPercentile(experience: Experience) {
    const x1 = --experience.ExperienceOnFoW1.Functions.length;
    const y1 = --experience.ExperienceOnFoW1.Specialities.length;
    const maxScore1 = maxScoreMap[x1][y1 <= 2 ? y1: 2];
    let maxScore2: number;

    if (experience.ExperienceOnFoW2 && experience.ExperienceOnFoW2.FieldOfWork) {
        const x2 = --experience.ExperienceOnFoW2.Functions.length;
        const y2 = --experience.ExperienceOnFoW2.Specialities.length;
        maxScore2 = maxScoreMap[x2][y2 <= 3 ? y2: 3];
    }

    const percentileCalculator = [
        {
            $addFields: {
                FoW1Match: {
                    $multiply: [{
                        $divide: [
                            "$FoW1Score",
                            maxScore1
                        ]
                    }, 100]
                },
                FoW2Match: {
                    $multiply: [{
                        $divide: [
                            "$FoW2Score",
                            maxScore2
                        ]
                    }, 100]
                }
            }
        },
        {
            $addFields: {
                OverallMatch: {
                    $cond: {
                        if: {
                            $gte: ["$FoW1Match", "$FoW2Match"]
                        },
                        then: "$FoW1Match",
                        else: "$FoW2Match"
                    }
                }
            }
        }
    ];
    return percentileCalculator;
}
