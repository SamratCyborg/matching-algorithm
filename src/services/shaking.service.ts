import {Experience} from "../models/i-profile";

export function extractNecessaryData(experience: Experience) {
    const aggregationStage = {
        $replaceRoot: {
            newRoot: {
                $mergeObjects: [{
                    _id: "$_id",
                    Info: {
                        Name: "$Name",
                        Address: {
                            Country: "$Address.Country",
                            PostCode: "$Address.PostCode"

                        },
                        CV: {
                            $arrayElemAt: [
                                {
                                    $filter: {
                                        input: "$Documents",
                                        as: "doc",
                                        cond: {
                                            $eq: ["$$doc.Type", "CV"]
                                        }
                                    }
                                },
                                0
                            ]
                        },
                        DateOfBirth: "$DateOfBirth",
                        HighestEducationEn: "$HighestEducationEn",
                        Distance: "$calculatedDistance",
                        FieldOfWork1: {
                            $cond: {
                                if: {$eq: [experience.ExperienceOnFoW1.FieldOfWork.ItemId, "$Experience.ExperienceOnFoW1.FieldOfWork.ItemId"]},
                                then: {
                                    Title: "$Experience.ExperienceOnFoW1.FieldOfWork.Title",
                                    Functions: {
                                        $map: {
                                            input: "$Experience.ExperienceOnFoW1.Functions",
                                            as: "item",
                                            in: "$$item.Activity.Title"
                                        }
                                    },
                                    Specialities: {
                                        $map: {
                                            input: "$Experience.ExperienceOnFoW1.Specialities",
                                            as: "item",
                                            in: "$$item.Activity.Title"
                                        }
                                    },
                                    Experience: "$Experience.ExperienceOnFoW1"
                                },
                                else: {
                                    $cond: {
                                        if: {$eq: [experience.ExperienceOnFoW1.FieldOfWork.ItemId, "$Experience.ExperienceOnFoW2.FieldOfWork.ItemId"]},
                                        then: {
                                            Title: "$Experience.ExperienceOnFoW2.FieldOfWork.Title",
                                            Functions: {
                                                $map: {
                                                    input: "$Experience.ExperienceOnFoW2.Functions",
                                                    as: "item",
                                                    in: "$$item.Activity.Title"
                                                }
                                            },
                                            Specialities: {
                                                $map: {
                                                    input: "$Experience.ExperienceOnFoW2.Specialities",
                                                    as: "item",
                                                    in: "$$item.Activity.Title"
                                                }
                                            },
                                            Experience: "$Experience.ExperienceOnFoW2"
                                        },
                                        else: {}
                                    }
                                }
                            }
                        },
                    }
                }
                ]
            }
        }
    }
    if (experience.ExperienceOnFoW2 && experience.ExperienceOnFoW2.FieldOfWork) {
        // @ts-ignore
        aggregationStage.$replaceRoot.newRoot.$mergeObjects[0].Info["FieldOfWork2"] = {
            $cond: {
                if: {$eq: [experience.ExperienceOnFoW2.FieldOfWork.ItemId, "$Experience.ExperienceOnFoW2.FieldOfWork.ItemId"]},
                then: {
                    Title: "$Experience.ExperienceOnFoW2.FieldOfWork.Title",
                    Functions: {
                        $map: {
                            input: "$Experience.ExperienceOnFoW2.Functions",
                            as: "item",
                            in: "$$item.Activity.Title"
                        }
                    },
                    Specialities: {
                        $map: {
                            input: "$Experience.ExperienceOnFoW2.Specialities",
                            as: "item",
                            in: "$$item.Activity.Title"
                        }
                    },
                    Experience: "$Experience.ExperienceOnFoW2"
                },
                else: {
                    $cond: {
                        if: {$eq: [experience.ExperienceOnFoW2.FieldOfWork.ItemId, "$Experience.ExperienceOnFoW1.FieldOfWork.ItemId"]},
                        then: {
                            Title: "$Experience.ExperienceOnFoW1.FieldOfWork.Title",
                            Functions: {
                                $map: {
                                    input: "$Experience.ExperienceOnFoW1.Functions",
                                    as: "item",
                                    in: "$$item.Activity.Title"
                                }
                            },
                            Specialities: {
                                $map: {
                                    input: "$Experience.ExperienceOnFoW1.Specialities",
                                    as: "item",
                                    in: "$$item.Activity.Title"
                                }
                            },
                            Experience: "$Experience.ExperienceOnFoW1"
                        },
                        else: {}
                    }
                }
            }
        };
    }
    return aggregationStage;

}

export function removeNullPoints(experience: Experience) {
    const projection = {
        _id: 1,
        Info: 1,
    };
    if (experience.ExperienceOnFoW1) {
        // @ts-ignore
        projection["FoW1"] = {
            SpecialityMatchResult: 1,
            FunctionMatchResult: 1,
            FunctionPoints: {
                $filter: {
                    input: "$FoW1.FunctionPoints",
                    as: "item",
                    cond: {$ne: ["$$item", null]}
                }
            },
            SpecialityPoints: {
                $filter: {
                    input: "$FoW1.SpecialityPoints",
                    as: "item",
                    cond: {$ne: ["$$item", null]}
                }
            }
        };
    }
    if (experience.ExperienceOnFoW2) {
        // @ts-ignore
        projection["FoW2"] = {
            SpecialityMatchResult: 1,
            FunctionMatchResult: 1,
            FunctionPoints: {
                $filter: {
                    input: "$FoW2.FunctionPoints",
                    as: "item",
                    cond: {$ne: ["$$item", null]}
                }
            },
            SpecialityPoints: {
                $filter: {
                    input: "$FoW2.SpecialityPoints",
                    as: "item",
                    cond: {$ne: ["$$item", null]}
                }
            }
        };
    }

    return projection;
}

export function removeThirdPoint(experience: Experience) {
    const addFields = {
        $addFields: {
            FoW1: {
                ThirdFunctionPoint: {
                    $cond: {
                        if: {$gt: [{$size: "$FoW1.FunctionPoints"}, 2]},
                        then: {
                            $min: "$FoW1.FunctionPoints"
                        },
                        else: null as any
                    }
                },
                ThirdSpecialityPoint: {
                    $cond: {
                        if: {$gt: [{$size: "$FoW1.SpecialityPoints"}, 2]},
                        then: {
                            $min: "$FoW1.SpecialityPoints"
                        },
                        else: null as any
                    }
                }
            }
        }
    }
    const projection = {
        $project: {
            _id: 1,
            Info: 1,
            FoW1: {
                FunctionPoints: {
                    $filter: {
                        input: "$FoW1.FunctionPoints",
                        as: "item",
                        cond: {$ne: ["$$item", "$FoW1.ThirdFunctionPoint"]}
                    }
                },
                SpecialityPoints: {
                    $filter: {
                        input: "$FoW1.SpecialityPoints",
                        as: "item",
                        cond: {$ne: ["$$item", "$FoW1.ThirdSpecialityPoint"]}
                    }
                },
                FunctionMatchResult: 1,
                SpecialityMatchResult: 1,
            }
        }
    }
    if (experience.ExperienceOnFoW2) {
        // @ts-ignore
        addFields.$addFields["FoW2"] = {
            ThirdFunctionPoint: {
                $cond: {
                    if: {$gt: [{$size: "$FoW2.FunctionPoints"}, 2]},
                    then: {
                        $min: "$FoW2.FunctionPoints"
                    },
                    else: null as any
                }
            },
            ThirdSpecialityPoint: {
                $cond: {
                    if: {$gt: [{$size: "$FoW2.SpecialityPoints"}, 2]},
                    then: {
                        $min: "$FoW2.SpecialityPoints"
                    },
                    else: null as any
                }
            }
        }
        // @ts-ignore
        projection.$project["FoW2"] = {
            FunctionPoints: {
                $filter: {
                    input: "$FoW2.FunctionPoints",
                    as: "item",
                    cond: {$ne: ["$$item", "$FoW2.ThirdFunctionPoint"]}
                }
            },
            SpecialityPoints: {
                $filter: {
                    input: "$FoW2.SpecialityPoints",
                    as: "item",
                    cond: {$ne: ["$$item", "$FoW2.ThirdSpecialityPoint"]}
                }
            },
            SpecialityMatchResult: 1,
            FunctionMatchResult: 1
        }
    }
    // @ts-ignore
    return [
        addFields,
        projection
    ];

}


