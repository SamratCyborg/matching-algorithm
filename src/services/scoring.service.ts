import {MatA, MatB, MatC} from "../configs/point-tables";
import {Experience, ExperienceOnFoW, PrioritizedActivity} from "../models/i-profile";

interface MatchingPointAggregation {
    FunctionPoints: {
        $arrayElemAt: any[];
    }[];
    SpecialityPoints: {
        $arrayElemAt: any[];
    }[];
}

function getAppropriatePointTable(experienceOnFieldOfWork: ExperienceOnFoW): {
    Function: number[][];
    Speciality: number[][];
} {
    const f = experienceOnFieldOfWork.Functions.length;
    const s = experienceOnFieldOfWork.Specialities.length;
    if (f === 1 && s === 1) {
        return {
            Function: MatA,
            Speciality: MatA
        };
    } else {
        return {
            Function: f === 1 ? MatC : MatB,
            Speciality: s === 1 ? MatC : MatB
        };
    }

}

function getMatchingPointsForFoW(fow: number, experienceOnFoW: ExperienceOnFoW): MatchingPointAggregation {
    const matchingPointAggregation: MatchingPointAggregation = {
        FunctionPoints: [],
        SpecialityPoints: []
    };
    const pointTables: {
        Function: number[][];
        Speciality: number[][];
    } = getAppropriatePointTable(experienceOnFoW);

    experienceOnFoW.Functions.forEach((activity: PrioritizedActivity, index: number) => {
        matchingPointAggregation.FunctionPoints.push({
            $arrayElemAt: [
                {$arrayElemAt: [pointTables.Function, index]},
                {
                    $cond: {
                        if: {$lt: [{$arrayElemAt: ["$FoW" + fow + ".FunctionMatchResult", index]}, 0]},
                        then: 30,
                        else: {$arrayElemAt: ["$FoW" + fow + ".FunctionMatchResult", index]}
                    }
                }
            ]
        });
    });

    experienceOnFoW.Specialities.forEach((activity: PrioritizedActivity, index: number) => {
        matchingPointAggregation.SpecialityPoints.push({
            $arrayElemAt: [
                {$arrayElemAt: [pointTables.Speciality, index]},
                {
                    $cond: {
                        if: {$lt: [{$arrayElemAt: ["$FoW" + fow + ".SpecialityMatchResult", index]}, 0]},
                        then: {
                            $cond: {
                                if: {$lt: [{$arrayElemAt: ["$FoW" + fow + ".SpecialityParentMatchResult", index]}, 0]},
                                then: 30,
                                else: {$arrayElemAt: ["$FoW" + fow + ".SpecialityParentMatchResult", index]}
                            }
                        },
                        else: {$arrayElemAt: ["$FoW" + fow + ".SpecialityMatchResult", index]}
                    }
                }
            ]
        });
    });

    return matchingPointAggregation;
}

export function getMatchPoints(searchCase: string, experience: Experience): {
    FoW1: MatchingPointAggregation;
    FoW2? : MatchingPointAggregation;
} {
    let pointMapperAggregation: any;
    if (experience.ExperienceOnFoW1) {
        pointMapperAggregation = {
            FoW1: getMatchingPointsForFoW(1, experience.ExperienceOnFoW1),
        };
    }
    if (experience.ExperienceOnFoW2) {
        pointMapperAggregation["FoW2"] = getMatchingPointsForFoW(2, experience.ExperienceOnFoW2);
    }
    return pointMapperAggregation;
}

function getAdjustedPoints(fow: number) {
    return {
        SpecialityPoints: {
            $map: {
                input: {
                    $range: [0, 3]
                },
                as: "index",
                in: {
                    $cond: {
                        if: {
                            $gte: [{$arrayElemAt: ["$FoW" + fow + ".SpecialityParentMatchResult", "$$index"]}, 0]
                        },
                        then: {
                            $multiply: [{
                                $arrayElemAt: ["$FoW" + fow + ".SpecialityPoints", "$$index"]
                            }, .9]
                        },
                        else: {
                            $arrayElemAt: ["$FoW" + fow + ".SpecialityPoints", "$$index"]
                        }
                    }
                }
            }
        },
        SpecialityMatchResult: {
            $map: {
                input: {
                    $range: [0, 3]
                },
                as: "index",
                in: {
                    $cond: {
                        if: {
                            $eq: [{$arrayElemAt: ["$FoW" + fow + ".SpecialityParentMatchResult", "$$index"]}, -1]
                        },
                        then: {
                            $arrayElemAt: ["$FoW" + fow + ".SpecialityMatchResult", "$$index"]

                        },
                        else: {
                            $arrayElemAt: ["$FoW" + fow + ".SpecialityParentMatchResult", "$$index"]
                        }
                    }
                }
            }
        }
    }
}

export function adjustAllCandidate(experience: Experience) {
    const stage = {
        $addFields: {
            FoW1: getAdjustedPoints(1)
        }
    }
    if (experience.ExperienceOnFoW2) {

        // @ts-ignore
        stage.$addFields["FoW2"] = getAdjustedPoints(2);
    }
    return stage;
}



export function addPoints(experience: Experience) {
    const addFields =  {
        $addFields: {
            FoW1: {
                FunctionScore: {
                    $cond: {
                        if: {$gte: [{$size: "$FoW1.FunctionPoints"}, 2]},
                        then: {
                            $add: [{$arrayElemAt: ["$FoW1.FunctionPoints", 0]}, {$arrayElemAt: ["$FoW1.FunctionPoints", 1]}]
                        },
                        else: {$arrayElemAt: ["$FoW1.FunctionPoints", 0]}
                    }
                },
                SpecialityScore: {
                    $cond: {
                        if: {$gte: [{$size: "$FoW1.SpecialityPoints"}, 2]},
                        then: {
                            $add: [{$arrayElemAt: ["$FoW1.SpecialityPoints", 0]}, {$arrayElemAt: ["$FoW1.SpecialityPoints", 1]}]
                        },
                        else: {$arrayElemAt: ["$FoW1.SpecialityPoints", 0]}
                    }
                }
            }
        }
    }

    const projection = {
        $project: {
            _id: 1,
            Info: 1,
            FoW1: 1,
            FoW2: 1,
            FoW1Score: {
                $add: ["$FoW1.FunctionScore", "$FoW1.SpecialityScore"]
            }
        }
    }

    if (experience.ExperienceOnFoW2 && experience.ExperienceOnFoW2.FieldOfWork) {
        // @ts-ignore
        addFields.$addFields["FoW2"] = {
            FunctionScore: {
                $cond: {
                    if: {$gte: [{$size: "$FoW2.FunctionPoints"}, 2]},
                    then: {
                        $add: [{$arrayElemAt: ["$FoW2.FunctionPoints", 0]}, {$arrayElemAt: ["$FoW2.FunctionPoints", 1]}]
                    },
                    else: {$arrayElemAt: ["$FoW2.FunctionPoints", 0]}
                }
            },
            SpecialityScore: {
                $cond: {
                    if: {$gte: [{$size: "$FoW2.SpecialityPoints"}, 2]},
                    then: {
                        $add: [{$arrayElemAt: ["$FoW2.SpecialityPoints", 0]}, {$arrayElemAt: ["$FoW2.SpecialityPoints", 1]}]
                    },
                    else: {$arrayElemAt: ["$FoW2.SpecialityPoints", 0]}
                }
            }
        };
        // @ts-ignore
        projection.$project.FoW2Score = {
            $add: ["$FoW2.FunctionScore", "$FoW2.SpecialityScore"]
        }
    }
    return  [
       addFields,
        projection
    ];

}


