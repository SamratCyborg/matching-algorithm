
export const MatchedJobProjection = {
    Tags: 1,
    ReferenceNumber: 1,
    Title: 1,
    Documents: 1,
    CreateDate: 1,
    Project: {
        _id: 1,
        Status: 1,
        CreateDate: 1
    },
    ApplicationsInActiveStatus: {
        _id: 1,
        Source: 1,
        Status: 1,
        CreateDate: 1
    },
    Publications: {
        PublishedOn: 1,
        PublishedAt: 1
    },
    LastActionByOwner: {
        ActionId: 1,
        ActionTitle: 1,
        _id: 1,
        CreateDate: 1
    },
    LastAssignment: {
        _id: 1,
        Source: 1,
        Status: 1,
        CreateDate: 1
    },
    EmployerPartial: {
        Name: 1,
        ItemId: 1,
        Logo: 1
    },
    Consultant: {
        DisplayName: 1,
        ProfileImage: 1,
        Email: 1,
        _id: 1
    }
}
