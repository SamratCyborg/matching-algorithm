export interface IAddress {
  FormattedAddress: string;
  State: string;
  PostCode: number;
  Country: string;
  City: string;
  PlaceId: string;
  // Latitude?: number;
  // Longitude?: number;
  Coordinates:number[];
  IsPrimary?: boolean;
}
