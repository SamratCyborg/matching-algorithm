export const PersonPartialProjection = {
    Name: 1,
    _id: 1,
    Email: 1,
    ProfileImage: 1,
    PhoneNumber: 1,
    DisplayName: 1
}
export const EmployerPartialProjection = {
    ItemId: 1,
    Name: 1,
    Logo: 1,
    Address: {
        FormattedAddress: 1,
        City: 1,
        PostCode: 1,
        Country: 1
    },
    Owners: PersonPartialProjection,
    ContactPersons: 1
}
export const JobPartialProjection = {
    _id: 1,
    Title: 1,
    ReferenceNumber: 1,
    Status: 1,
    EmployerPartial: EmployerPartialProjection,
    Owner: PersonPartialProjection
}

export const CandidatePartialProjection = {
    Name: 1,
    Owners: PersonPartialProjection,
    CurrentOrLastEmployer: 1,
    Phone: 1,
    Remarks: 1,
    _id: 1
}
export const JobApplicationProjection = {
    Tags: 1,
    JobId: 1,
    CandidatePartial: CandidatePartialProjection,
    Owners: PersonPartialProjection,
    DocumentPartials: 1,
    Source: 1,
    Status: 1,
    Comments: 1,
    _id: 1,
}
export const JobProjectProjection = {
    Tags: 1,
    JobPartial: JobPartialProjection,
    ApplicationIds: 1,
    Status: 1,
    Comments: 1,
    History: 1,
    CreateDate: 1,
    QuickAppMailTitle: 1,
    JobApplications: {
        _id: 1,
        CandidatePartial: {
            Name: 1,
            _id: 1
        },
        Status: 1
    }
}
