import {EntityBase} from "./entity-base";
import {JobApplicationPartial, UserPartial} from "./denormalized-views";

export interface ApplicationHistory extends EntityBase {
    ApplicationPartial: JobApplicationPartial;
    Action: string;
    Subject: string;
    Status: string;
    PerformedBy: UserPartial;
    Description: string;
}
