export interface Profile {
    SalaryRanges: string[];
    Rating: string;
    Budget: string;
    Keywords: string[];
    Languages: LanguageSkill[];
    Experience: Experience;
    FieldOfWorks: Criterion[];
    Leadership: string;
    Type: JobCompanyType;
    ManagementEnabled: boolean;
}


export interface Experience {
    ExperienceOnFoW1: ExperienceOnFoW;
    ExperienceOnFoW2?: ExperienceOnFoW;
}


export interface ExperienceOnFoW {
    FieldOfWork: Criterion;
    Functions: PrioritizedActivity[];
    Specialities: PrioritizedActivity[];
    RequiredActivities: string[];
    ActivityIds: string[];
    FunctionIds: string[];
    SpecialityIds: string[];
    SpecialityParents?: string[];
}


export interface PrioritizedActivity {
    Activity: Criterion;
    Priority: number;
}

export interface Criterion {
    ItemId?: string;
    Parent?: string | Criterion;
    Title: string;
    Path?: string[];
    Type?: string;
}

export interface JobCompanyType {
    CompanyType: string;
    ManagementEnabled: boolean;
}

export interface LanguageSkill {
    Name: string;
    ProficiencyLevel: string;
}



