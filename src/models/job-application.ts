import {EntityBase} from "./entity-base";
import {CandidatePartial, DocumentPartial} from "./denormalized-views";

export interface JobApplication extends EntityBase {
    CandidatePartial: CandidatePartial;
    Source: string;
    DocumentPartials: DocumentPartial[];
    JobId: string;
    Comments: string;
    Status: string;
}


export enum assignmentSources {
    Application = 'application',
    Recommendation = 'recommendation',
    Assignment = 'assignment'
};
