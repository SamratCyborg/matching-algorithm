import {IAddress} from "./address";

export interface ICustomerPartial {
    Name: string;
    Address: IAddress;
    Logo: string;
    ContactPersons: string[];
    Consultants: string[];
    ConsultantList?: PersonPartial[];
    ItemId?: string;
    TotalJobs?: number;
    NumberOfActiveJobs?: number;
    Addition?: string;

}
export interface UserPartial {
    Name: string;
    Email: string;
    _id: string;
}

export interface JobPartial {
    Title: string;
    ReferenceNumber: number;
    _id: string;
    Status: string;
    EmployerPartial: ICustomerPartial;
    Consultant: string;
}

export interface PersonPartial {
    Name: string;
    _id: string;
    Email: string;
    ProfileImage: string;
    PhoneNumber: string;
    ItemId?: string;
    DisplayName?: string;
}

export interface CandidatePartial {
    Name: string;
    Consultants: PersonPartial[];
    _id: string;
    CurrentOrLastEmployer: string;
    Phone: string;
    Remarks: string;
}

export interface DocumentPartial {
    _id: string;
    Name: string;
    FileId: string;
    Type: string;
}

export interface JobApplicationPartial {
    CandidatePartial: CandidatePartial;
    _id: string;
}

