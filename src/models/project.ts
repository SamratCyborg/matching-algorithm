import {EntityBase} from "./entity-base";
import {JobPartial, UserPartial} from "./denormalized-views";

export interface Project extends EntityBase {
    JobPartial: JobPartial;
    ApplicationIds: string[];
    Comments: string;
    Status: string;
    History: ProjectActionHistory[];
}


export interface ProjectActionHistory {
    Date: Date;
    PerformedBy: UserPartial;
    Description: string;
    Action: string;
    CandidateIds: string[];
}
