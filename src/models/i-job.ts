import {Address} from "./i-candidate";
import {Criterion, Profile} from "./i-profile";

export interface Job {
    EmployerPartial: any;
    Title: string;
    Address: Address;
    Status?: string;
    ProjectStatus?: string;
    FieldOfWorks: Criterion[];
    Documents?: any[];
    Consultant: string | any;
    ContactPerson: string | any;
    Tags?: string[];
    ItemId?: string;
    Match?: number;
    ZW?: number;
    CreateDate?: any;
    Workplace: string;
    ReferenceNumber?: string;
    Addition?: string;
    Advertisement?: string;
    AdvertisementArchive?: string;
    City?: string;
    Language?: string;

    IsPublished?: boolean;
    ChangeHistory?: {
        ChangedAt: Date;
        Type: string; // publication, advertisement
        ChangedBy: {
            ItemId: string;
            Name: string;
        };
        Changes: {
            FieldName: string;
            ChangeObject: string; // stringified json {PreviousValue: any, CurrentValue: any}
        }[];
    }[];

}

export interface ProfiledJob extends Job, Profile {
    // Workplace: Workplace;

}

export interface PublishedJob extends ProfiledJob {
    PublishedAt: Date;
    Publications: {
        PublishedOn: string;
        PublishedAt: Date;
        Data: string;
    }[];
}

