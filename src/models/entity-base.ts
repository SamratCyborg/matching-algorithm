export interface EntityBase {
    CreatedBy?: string;
    CreateDate?: Date;
    ItemId: string;
    Language?: string;
    LastUpdateDate?: Date;
    LastUpdatedBy?: string;
    Tags: string[];
    IsMarkedToDelete?: boolean;
}
