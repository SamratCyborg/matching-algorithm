export interface Page {
    PageNumber: number;
    PageSize: number;
}
