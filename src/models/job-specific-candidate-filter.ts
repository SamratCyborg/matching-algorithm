import {LanguageSkill} from "./i-profile";

export interface JobSpecificCandidateFilter {
    JobType: string;
    CandidateStatus: string;
    Country: string | any;
    Language1: LanguageSkill;
    Language2: LanguageSkill;
    Distance: number;
    PeopleManagement: string | any;
    Budget: string | any;
    Rating: string | any;
    CvReg: string | any;
    Age: string | any;
    PostCodeFrom: string;
    PostCodeTo: string;
    Keyword: string;
}

