import {LanguageSkill, Profile} from "./i-profile";

export interface BasicCandidate {
    UserId?: string;
    ItemId?: string;
    FirstName: string;
    LastName: string;
    Name?: string;
    MessageFromCandidate?: string;
    Salutation: string;
    Email: string;
    Address: Address;
    Phone: string;
    Consultants: string[];
    ConsultantList: any[];
    DateOfBirth: Date | string;
    SeeMoreConsultantToolTipText?: string;
    Source?: string;
    Tags?: string[];
    Status?: string;
    Documents: any[];

}

export interface CandidateAdditonal {
    PreferredJobType?: string;
    FieldOfActivities?: string[];
    Nationality: string;
    CurrentOrLastEmployer: string;
    CivilStatus: string;
    Languages: LanguageSkill[];
    Remarks?: string;
    HighestEducationEn: string;
    HighestEducationDe?: string;
    HighestEducationFr?: string;
    ProfilePicture?: string;
    Tags?: string[];
}

export interface Candidate extends BasicCandidate, CandidateAdditonal {
    CreatedBy?: string;
    ProfileSheet?: any;
    pictureUrl?: string;
    Language?: string;
}


export interface ProfiledCandidate extends Candidate, Profile {

}

export interface Address {
    FormattedAddress: string;
    State: string;
    PostCode: number;
    Country: string;
    City: string;
    PlaceId: string;
    Coordinates: number[];
    IsPrimary?: boolean;
}


