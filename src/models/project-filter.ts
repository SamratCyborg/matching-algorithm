export interface ProjectFilter {
    Customer?: {
        ItemId?: string,
        SearchKey: string
    };
    Job?: {
        ItemId?: string,
        SearchKey: string,
        ReferenceNumber?: number
    };
    Candidate?: {
        ItemId?: string,
        SearchKey: string
    };
    CandidateOwner?: string;
    ApplicationStatus?: number;
    JobOwner?: string;
    CustomerOwner?: string;
    IsRecommended?: boolean;
    IsActive?: boolean;
    ApplicationIds?: string[];
    ProjectStatus?: string;
    DateFrom?: Date;
    DateTo?: Date;
}
