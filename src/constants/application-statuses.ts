export enum ApplicationStatusEnum {
    PLACED = 1,
    SHORT_LISTED = 2,
    SENT = 3,
    QUICK_APP = 4,
    FIRST_CHOICE = 5,
    WATCHLIST = 6,
    APPLICATION = 7,
    REJECTION_RECRUITER = 8,
    REJECTION_CLIENT = 9,
    REJECTION_CANDIDATE = 10,
    DELETED = 11
}
