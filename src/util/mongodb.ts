import mongoose, { Connection } from "mongoose";
import { MONGODB_URI } from "./secrets";

mongoose.Promise = global.Promise;

export class MongoDB {
    private static connection = mongoose.connection as any;
    public static async connect(): Promise<string> {
        const options = { keepAlive: 1, connectTimeoutMS: 30000, reconnectTries: 30, reconnectInterval: 5000, poolSize: 5000 };
        const mongoUrl = MONGODB_URI;
        const connection = mongoose.connection as any;
        if (connection.readyState) {
            return mongoUrl;
        } else {
            try {
                const connectionUrl =  await mongoose.connect(mongoUrl, options as  any);
                return connectionUrl as any;
            } catch (e) {
                throw e;
            }
        }

    }

    public static async createConnection(tenantId?: string): Promise<Connection> {
        const options = { keepAlive: 1,  poolSize: 500 };
        const mongoUrl = MONGODB_URI;
        debugger
        return mongoose.createConnection(mongoUrl, options as any);
    }
}
