export function addMinutes(date: Date, minutes: number) {
  return new Date(date.getTime() + minutes * 60 * 1000);
}


export function subtractMinutes(date: Date, minutes: number) {
    return new Date(date.getTime() - minutes * 60 * 1000);
}
