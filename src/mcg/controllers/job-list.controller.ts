// @ts-ignore
import {Request, Response} from "@types/express";
import {IJobFilter} from "../models/mcg-job";
import {Repository} from "../../repository/repository";
import {McgCandidate} from "../models/mcg-candidate";
import {mcgJobProjections} from "../projections/job-projections";


export async function getJobList(req: Request, res: Response) {
    const jobFilter: IJobFilter = req.body;
    const skip = (jobFilter.PageNumber * jobFilter.PageSize) || 0;
    const limit = jobFilter.PageSize || 10;
    const jobRepo = new Repository("McgJob");
    const matchJobFilter = {} as any;
    if (jobFilter.Search) {
        matchJobFilter.Title = new RegExp(jobFilter.Search, 'gi');
    }
    if (jobFilter.Function) {
        matchJobFilter.Function = jobFilter.Function;
    }

    if (jobFilter.Sectors && jobFilter.Sectors.length) {
        matchJobFilter['Organization.Sector'] = {$in: jobFilter.Sectors}
    }

    if (jobFilter.PreferredCompanies && jobFilter.PreferredCompanies.length) {
        matchJobFilter['Organization._id'] = {$in: jobFilter.PreferredCompanies};
    }

    const aggregationStages: any[] = [];
    aggregationStages.push({
        $match: matchJobFilter
    });

    const interestLookupFilter = {
        $expr: {$eq: ["$$jobId", "$Job._id"]},
    } as any;


    if (jobFilter.IsACandidate) {
        const userId = jobFilter.UserId;
        const candidateId = await getCandidateIdFromUserId(userId);
        interestLookupFilter['Candidate._id'] = candidateId;
    }

    const candidateLookupAggregation = {
        $lookup: {
            from: "CandidateJobInterests",
            let: {jobId: "$_id"},
            pipeline: [
                {
                    $match: interestLookupFilter
                }
            ],
            as: "Interests"
        }
    }

    aggregationStages.push(candidateLookupAggregation);
 /*   aggregationStages.push({
        $match: {
            $expr: {$gt: [{$size: "$Interests"}, 0]}
        }
    })*/
 const paginatedResult: any[] = [];

    paginatedResult.push({
        $skip: skip
    });
    paginatedResult.push(
        {
            $limit: limit
        });
    paginatedResult.push({
        $project: mcgJobProjections
    })

    const paginatorAggregation = {
        $facet: {
            TotalCount: [{
                "$count": "Count"
            }],
            Data: paginatedResult,
        }
    }

    aggregationStages.push(paginatorAggregation);
    aggregationStages.push({
        $unwind: "$TotalCount"
    });
    aggregationStages.push({
        $project: {
            TotalCount: "$TotalCount.Count",
            Data: 1
        }
    })


    console.log(aggregationStages)
    const result = await jobRepo.aggregate(aggregationStages);

    res.send({
        Success: true,
        Result: result[0]
    })

}

async function getCandidateIdFromUserId(userId: string) {
    const candidateRepo = new Repository("McgCandidate");
    const candidate: McgCandidate = await candidateRepo.findOne({UserId: userId});
    if (candidate && candidate._id) {
        return candidate._id;
    } else {
        throw "No candidate found";
    }

}
