// @ts-ignore
import {Request, Response} from "@types/express";
import {IJobFilter} from "../models/mcg-job";
import {Repository} from "../../repository/repository";
import {McgCandidate} from "../models/mcg-candidate";
import {mcgJobProjections} from "../projections/job-projections";
import {IOrganizationFilter} from "../models/mcg-organization";
import {mcgOrgProjections} from "../projections/org-projections";


export async function getOrganizationList(req: Request, res: Response) {
    const organizationFilter: IOrganizationFilter = req.body;
    const skip = (organizationFilter.PageNumber * organizationFilter.PageSize) || 0;
    const limit = organizationFilter.PageSize || 10;
    const orgRepo = new Repository("McgOrganization");

    const aggregationStages: any[] = [];
    if (organizationFilter.PreferredCompanies && organizationFilter.PreferredCompanies.length) {
        aggregationStages.push({
            $match: {
                _id: {
                    $in: organizationFilter.PreferredCompanies
                }
            }
        })
    }

    const jobLookUpFilter = {
        $expr: {$eq: ["$$orgId", "$Organization._id"]},
        IsMarkedToDelete: false
    } as any;


    const jobLookupAggregation = {
        $lookup: {
            from: "McgJobs",
            let: {orgId: "$_id"},
            pipeline: [
                {
                    $match: jobLookUpFilter
                }
            ],
            as: "Jobs"
        }
    }

    aggregationStages.push(jobLookupAggregation);

    const interestLookFilter = {
        $expr: {$eq: ["$$orgId", "$Job.Organization._id"]},
        IsMarkedToDelete: false
    } as any;
    if (organizationFilter.UserId) {
        const userId = organizationFilter.UserId;
        const candidateId = await getCandidateIdFromUserId(userId);
        interestLookFilter['Candidate._id'] = candidateId;
    }

    const interestLookupAggregation = {
        $lookup: {
            from: "CandidateJobInterests",
            let: {orgId: "$_id"},
            pipeline: [
                {
                    $match: interestLookFilter
                }
            ],
            as: "Interests"
        }
    }

    aggregationStages.push(interestLookupAggregation);

    aggregationStages.push({
        $project: {
            ...mcgOrgProjections,
            AvailableJobs: {$size: "$Jobs"},
            InterestedInJobs: {$size: "$Interests"}
        }
    })


    const paginatedResult: any[] = [];

    paginatedResult.push({
        $skip: skip
    });
    paginatedResult.push(
        {
            $limit: limit
        });
    paginatedResult.push({
        $project: mcgJobProjections
    })

    const paginatorAggregation = {
        $facet: {
            TotalCount: [{
                "$count": "Count"
            }],
            Data: paginatedResult,
        }
    }

    aggregationStages.push(paginatorAggregation);
    aggregationStages.push({
        $unwind: "$TotalCount"
    });
    aggregationStages.push({
        $project: {
            TotalCount: "$TotalCount.Count",
            Data: 1
        }
    })
    const result = await orgRepo.aggregate(aggregationStages);

    res.send({
        Success: true,
        Result: result[0]
    })

}

async function getCandidateIdFromUserId(userId: string) {
    const candidateRepo = new Repository("McgCandidate");
    const candidate: McgCandidate = await candidateRepo.findOne({UserId: userId});
    if (candidate && candidate._id) {
        return candidate._id;
    } else {
        throw "No candidate found";
    }

}
