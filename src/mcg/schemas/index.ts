
import {Schema} from "mongoose";
import {CandidateJobInterestSchema} from "./candidate-job-interest.schema";
import {McgJobSchema} from "./job.schema";
import {McgCandidateSchema} from "./candidate.schema";
import {OrganizationSchema} from "./organization.schema";

export const McgSchemaDefinitions: {[EntityName: string]: Schema} =  {
    McgJob: McgJobSchema,
    McgCandidate: McgCandidateSchema,
    McgOrganization: OrganizationSchema,
    CandidateJobInterest: CandidateJobInterestSchema
}
