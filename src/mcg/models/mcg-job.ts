import {EntityBase} from "../../models/entity-base";
import {IAddress} from "../../models/address";
import {IOrganizationPartial} from "./mcg-organization";
import {Page} from "../../models/request";

export interface McgJob extends EntityBase{
    Title: string;
    Organization: IOrganizationPartial;
    Sector: string;
    Function?: string;
    Address?: IAddress;
    Description: string;
}

export interface IJobFilter extends Page{
    Search?: string;
    Sectors?: string[];
    Function?: string;
    PreferredCompanies?: string[];
    IsACandidate?: boolean;
    UserId?: string;
}

export interface IJobPartial {
    _id: string;
    Title: string;
    Organization: IOrganizationPartial;
    Sector: string;
}
