export interface IProfileStepOne {
  DesiredSkills?: string[];
  UndesiredSkills?: string[];
  DesiredCompanyValues?: string[];
  PersonalStrengths?: string[];
  Tags?: string[];
}


export interface IProfileStepTwo {
  JobLocations?: string[];
  WageRequirement: number;
  DateOfBirth: Date | string;
  CurrentJobSatisfaction?: string;
  CommunicationLanguage?: string;
  ReceiveJobOpportunities?: boolean;
  ReceiveJobNews?: boolean;
  ProfilePicture?: string;
  Cv?: string;
  LnProfileLink?: string;
  Video?: string;
  Tags?: string[];
}

export interface IProfileStepThree {
  Functions?: string[];
  PreferredCompanies?: string[];
  Tags?: string[];
}


export interface IProfile extends IProfileStepOne, IProfileStepTwo, IProfileStepThree {
  Tags: string[];
}
