import {IAddress} from "../../models/address";
import {PersonPartial} from "../../models/denormalized-views";
import {ICandidatePartial} from "./mcg-candidate";
import {Page} from "../../models/request";

export interface McgOrganization {
    ItemId?: string;
    Name?: string;
    OrganizationId?: string;
    Logo?: string;
    Address?: IAddress;
    Sector?: string;
    Function?: string;
    ContactPerson: PersonPartial;
    Administrators?: PersonPartial[];
    InterestedCandidates: ICandidatePartial[];
    Tags?: string[];
    LogoUrl?: string;
    IsActive?: boolean;
}

export interface IOrganizationPartial {
    _id?: string;
    Name?: string;
    Logo?: string;
}

export interface IOrganizationFilter  extends Page{
    UserId: string;
    PreferredCompanies: string[];
}
