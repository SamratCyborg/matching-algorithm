import {IProfile, IProfileStepOne} from "./iprofiles";
import {Person} from "./person";
import {IOrganizationPartial} from "./mcg-organization";


export interface IBasicCandidate extends IProfileStepOne, Person {
    ItemId?: string;
    Name?: string;
    FirstName?: string;
    LastName?: string;
    Salutation?: string;
    Sex?: string;
    Designation?: string;
    UserId: string;
    Email: string;
    Language: string;
    Tags: string[];
    _id?: string;
}

export interface McgCandidate extends IBasicCandidate, IProfile {
    OrganizationsConacted?: IOrganizationPartial[];
    CandidatesSerial?: string[];
}

export interface ICandidatePartial {
    _id: string;
    Name: string;
    UserId?: string;
    ProfilePicture?: string;
}

export interface ICandidateFilter {
    Search?: any;
    HasContacted?: string[];
}

export interface SendMailByTemplateFilePayload {
    ToPersons: string[];
    ReplyTo?: string;
    Subject: string;
    BodyTemplateFileId: string;
}
