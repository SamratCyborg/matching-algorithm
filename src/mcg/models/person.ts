export interface Person {
    DisplayName?: string;
    FirstName?: string;
    LastName?: string;
    PhoneNumber?: string;
    OfficialPhoneNumber?: string;
    Email?: string;
    Sex?: string;
    Salutation?: string;
    ItemId?: string;
    Tags?: string[];
    Designation?: string;
    AddressLine1?: string;
    PostalCode?: number;
    ProfileImage?: string;
    ProfileImageUrl?: string;
    City?: string;
}
