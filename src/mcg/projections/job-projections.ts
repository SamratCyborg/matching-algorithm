export const mcgJobProjections  = {
    _id: 1,
    CreateDate: 1,
    Tags: 1,
    Title: 1,
    Address: 1,
    Description: 1,
    Function: 1,
    Organization: 1,
    Interests : {
        Candidate: 1,
        Job: 1,
        ContactData: 1,
        HasContacted: 1,
        HasViewed: 1,
        ViewData: 1
    }
}
