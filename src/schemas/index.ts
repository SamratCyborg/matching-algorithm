import {JobSchema} from "./job.schema";
import {CandidateSchema} from "./candidate.schema";
import {Schema} from "mongoose";
import {JobProjectSchema} from "./job-project.schema";
import {JobApplicationSchema} from "./job-application.schema";

export const SchemaDefinitions: {[EntityName: string]: Schema} =  {
    Job: JobSchema,
    Candidate: CandidateSchema,
    JobProject: JobProjectSchema,
    JobApplication: JobApplicationSchema
}
